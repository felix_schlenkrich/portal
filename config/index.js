const env = process.env.NODE_ENV || 'development';

// eslint-disable-next-line import/no-dynamic-require
const configEnv = require(`./config.${env}`);
const configGlobal = require('./config.global');

const config = Object.assign({}, configGlobal, configEnv);

module.exports = config;
