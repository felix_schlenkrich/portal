const config = {};

config.browserCacheMaxAge = '365d';
config.publicAssetsPath = './public';
config.buildOutputPath = './build';

config.site = {
  title: 'ad-monk',
};

config.auth = {
  secret: 'MyAwesomeSecret',
};

config.network = {
  protocol: 'http',
  host: 'localhost',
  port: 3000,
  portBundleServer: 4000,
  portAPI: 5000,
};

config.mailgun = {
  apiKey: 'key-0eb5449e69741c8d3ee62fedbf850658',
  domain: 'sandboxd24874c2cb984c1ab9ed488dcd1aea01.mailgun.org',
  from: 'ad-monk.com <info@ad-monk.com>',
};

config.network.getClientUrl = () =>
  `${config.network.protocol}://${config.network.host}:${config.network.port}`;

config.network.getApiUrl = () =>
  `${config.network.protocol}://${config.network.host}:${config.network.portAPI}`;

config.db = {
  database: 'portal',
  host: 'localhost',
  port: 3306,
  connectionLimit: 20,
  connectionString: 'mongodb://localhost:27017/portal',
};

config.bundleConfig = {
  client: {
    entry: './src/client/index.js',
    source: ['./src/client', './src/shared', './src/client'],
    outputPath: './build/client',
    webPath: '/client/',
  },
  express: {
    entry: './src/server/app.js',
    source: ['./src/server', './src/shared', './config'],
    output: '/build/server',
    filename: 'server.js',
    debugPort: 9230,
  },
};

config.additionalNodeBundles = {
  api: {
    entry: './src/api/index.js',
    source: ['./src/api', '.src/shared', './config'],
    outputPath: './build/api',
    filename: 'api.js',
    debugPort: 9231,
  },
};

module.exports = config;
