const config = {};

config.db = {
  database: 'portal',
  host: 'localhost',
  port: 3306,
  connectionLimit: 20,
  connectionString: 'mongodb://localhost:27017/portal-test',
};

module.exports = config;
