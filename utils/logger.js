import winston from 'winston';

let loglevel = 'info';

if (process.env.NODE_ENV === 'development') loglevel = 'debug';
if (process.env.NODE_ENV === 'test') loglevel = 'warn';

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: loglevel,
      humanReadableUnhandledException: true,
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true,
    }),
  ],
});

export default logger;
