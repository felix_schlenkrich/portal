import config from '../config';

const generatePageTitle = page => `${config.site.title} - ${page}`;

export default generatePageTitle;
