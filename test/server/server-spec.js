/* global it, describe, beforeEach */
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../../src/server/app';

process.env.NODE_ENV = 'test';
const config = require('../../config');

const expect = chai.expect;

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);

// eslint-disable-next-line no-unused-vars
// let server;

describe('Express Web Server', () => {
  beforeEach(() => {
    // eslint-disable-next-line global-require
    // server = require('../../src/server/app');
  });

  it('it should return status code 200', (done) => {
    chai
      .request(server)
      .get('/')
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it('it should have content-type of text/html', (done) => {
    chai
      .request(server)
      .get('/')
      .end((err, res) => {
        expect(res.header['content-type'])
          .to.be.an('string')
          .that.includes('text/html');
        done();
      });
  });
});
