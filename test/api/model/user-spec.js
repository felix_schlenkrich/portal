/* global it, describe, before */
/* eslint-disable no-unused-expressions, no-shadow, no-unused-vars */
import { expect } from 'chai';
import bluebird from 'bluebird';
import mongoose from 'mongoose';
import mochaMongoose from 'mocha-mongoose';
import User from '../../../src/api/model/user';

process.env.NODE_ENV = 'test';
const config = require('../../../config');

mongoose.Promise = bluebird;

const clearDb = mochaMongoose(config.db.connectionString, { noClear: true });

describe('Database: User Model', () => {
  before((done) => {
    if (mongoose.connection.db) {
      clearDb((err) => {
        expect(err).to.not.exist;
        done();
      });
    } else {
      mongoose.connect(config.db.connectionString, { useMongoClient: true }).then((db) => {
        clearDb((err) => {
          expect(err).to.not.exist;
          done();
        });
      });
    }
  });

  it('it can be saved', (done) => {
    new User({
      email: 'john.doe@felis.me',
      username: 'joehndoe',
      password: 'test',
      profile: { firstName: 'John', lastName: 'Doe' },
    })
      .save()
      .then(() => {
        done();
      })
      .catch((err) => {
        expect(err).to.not.exist;
        done(err);
      });
  });

  it('it can be listed', (done) => {
    User.find({})
      .exec()
      .then((models) => {
        expect(models).to.have.length(1);
        done();
      })
      .catch((err) => {
        expect(err).to.not.exist;
        done(err);
      });
  });

  it('it can be removed', (done) => {
    User.count((err, count) => {
      expect(err).to.not.exist;
      expect(count).to.equal(1);

      clearDb((err) => {
        expect(err).to.not.exist;

        User.find({})
          .exec()
          .then((docs) => {
            expect(docs.length).to.equal(0);
            done();
          })
          .catch((err) => {
            expect(err).to.not.exist;
            done(err);
          });
      });
    });
  });
});
