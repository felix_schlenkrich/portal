/* global it, describe, before, beforeEach */
/* eslint-disable no-unused-expressions, no-shadow */
import chai, { expect, assert } from 'chai';
import chaiHttp from 'chai-http';
import mongoose from 'mongoose';
import User from '../../../src/api/model/user';
import server from '../../../src/api';
import { BAD_REQUEST, CREATED } from '../../../utils/httpStatusCodes';

process.env.NODE_ENV = 'test';
const config = require('../../../config');

chai.use(chaiHttp);

describe('API: /api/auth/signup', () => {
  before((done) => {
    mongoose.connect(config.db.connectionString, { useMongoClient: true }).then((db) => {
      done();
    });
  });
  beforeEach((done) => {
    mongoose.connection.db.dropDatabase(() => {
      done();
    });
  });

  it('should not be able to signup', (done) => {
    chai.request(server).post('/api/auth/signup').end((err, res) => {
      expect(res).to.have.status(BAD_REQUEST);
      done();
    });
  });

  it('should retrieve jwt token', (done) => {
    chai
      .request(server)
      .post('/api/auth/signup')
      .send({
        email: 'john.doe@felis.me',
        username: 'johndoe',
        password: 'test',
        profile: {
          firstName: 'John',
          lastName: 'Doe',
        },
      })
      .end((err, res) => {
        expect(res).to.have.status(CREATED);
        expect(res.body.token).to.be.a('string');
        assert(res.body.token.length > 10, 'token is filled');
        assert(res.body.token.startsWith('Bearer'), 'token begins with Bearer');
        done();
      });
  });

  it('should create an user object in database', (done) => {
    chai
      .request(server)
      .post('/api/auth/signup')
      .send({ email: 'john.doe@felis.me', password: 'test', username: 'joehndoe' })
      .end((err, res) => {
        assert(res.body.user.email === 'john.doe@felis.me');
        User.find({})
          .exec()
          .then((models) => {
            expect(err).to.not.exist;
            expect(models).to.have.length(1);
            done();
          })
          .catch((err) => {
            expect(err).to.not.exist;
            done(err);
          });
      });
  });
});
