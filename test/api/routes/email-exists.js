/* global it, describe, before,beforeEach */
/* eslint-disable no-unused-expressions, no-shadow */
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import mongoose from 'mongoose';
import server from '../../../src/api/index';
import User from '../../../src/api/model/user';
import { OK } from '../../../utils/httpStatusCodes';

process.env.NODE_ENV = 'test';
const config = require('../../../config');

chai.use(chaiHttp);

describe('API /auth/email-exists', () => {
  before((done) => {
    mongoose.connect(config.db.connectionString, { useMongoClient: true }).then((db) => {
      done();
    });
  });

  beforeEach((done) => {
    mongoose.connection.db.dropDatabase(() => {
      done();
    });
  });

  it('should response with email not exists', (done) => {
    chai.request(server).post('/api/auth/email-exists').end((err, res) => {
      expect(res).to.have.status(OK);
      expect(res.body.taken).to.be.equal(false);
      done();
    });
  });
  it('should response with email already exists', (done) => {
    new User({
      email: 'john.doe@felis.me',
      username: 'johndoe',
      password: 'test',
      profile: { firstName: 'John', lastName: 'Doe' },
    })
      .save()
      .then(() => {
        chai
          .request(server)
          .post('/api/auth/email-exists')
          .send({ email: 'john.doe@felis.me' })
          .end((err, res) => {
            expect(res).to.have.status(OK);
            expect(res.body.taken).to.be.equal(true);
            done();
          });
      })
      .catch((err) => {
        expect(err).to.not.exist;
        done(err);
      });
  });
});
