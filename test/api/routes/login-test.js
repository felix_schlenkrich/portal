/* global it, describe, before, beforeEach */
/* eslint-disable no-unused-expressions, no-shadow */
import chai, { expect, assert } from 'chai';
import chaiHttp from 'chai-http';
import mongoose from 'mongoose';
import server from '../../../src/api';
import User from '../../../src/api/model/user';
import { BAD_REQUEST, OK } from '../../../utils/httpStatusCodes';

process.env.NODE_ENV = 'test';
const config = require('../../../config');

chai.use(chaiHttp);

describe('API: /api/auth/login', () => {
  before((done) => {
    mongoose.connect(config.db.connectionString, { useMongoClient: true }).then((db) => {
      done();
    });
  });

  beforeEach((done) => {
    mongoose.connection.db.dropDatabase(() => {
      done();
    });
  });

  it('should not be able to login', (done) => {
    chai.request(server).post('/api/auth/login').end((err, res) => {
      expect(res).to.have.status(BAD_REQUEST);
      done();
    });
  });

  it('should retrieve jwt token', (done) => {
    new User({
      email: 'john.doe@felis.me',
      username: 'johndoe',
      password: 'test',
      profile: { firstName: 'John', lastName: 'Doe' },
    })
      .save()
      .then(() => {
        chai
          .request(server)
          .post('/api/auth/login')
          .send({ email: 'john.doe@felis.me', password: 'test' })
          .end((err, res) => {
            expect(res).to.have.status(OK);
            expect(res.body.token).to.be.a('string');
            assert(res.body.token.length > 10, 'token is filled');
            assert(res.body.token.startsWith('Bearer'), 'token begins with Bearer');
            done();
          });
      })
      .catch((err) => {
        expect(err).to.not.exist;
        done(err);
      });
  });

  it('should retrieve an user object', (done) => {
    new User({
      email: 'john.doe@felis.me',
      username: 'johndoe',
      password: 'test',
      profile: { firstName: 'John', lastName: 'Doe' },
    })
      .save()
      .then(() => {
        chai
          .request(server)
          .post('/api/auth/login')
          .send({ email: 'john.doe@felis.me', password: 'test' })
          .end((err, res) => {
            assert(res.body.user.email === 'john.doe@felis.me');
            done();
          });
      })
      .catch((err) => {
        expect(err).to.not.exist;
        done(err);
      });
  });
});
