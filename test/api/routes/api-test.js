/* global it, describe */
/* eslint-disable no-unused-expressions, no-shadow */
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import server from '../../../src/api/index';
import { OK } from '../../../utils/httpStatusCodes';

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);

describe('Express API Server', () => {
  it('it should return status code 200', (done) => {
    chai.request(server).post('/api/base/ping').end((err, res) => {
      expect(res).to.have.status(OK);
      done();
    });
  });

  it('it should have content-type of application/json', (done) => {
    chai.request(server).post('/api/base/ping').end((err, res) => {
      expect(res.header['content-type']).to.be.an('string').that.includes('application/json');
      done();
    });
  });
});
