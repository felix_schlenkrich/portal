/* global it, describe */
import { expect } from 'chai';

import reducer from '../../../src/shared/redux/reducer/auth';
import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR } from '../../../src/shared/redux/actions/types';

describe('Authentication reducer', () => {
  it('handles AUTH_LOGIN', (done) => {
    const action = { type: AUTH_LOGIN };
    const nextState = reducer(null, action);
    expect(nextState.authenticated).to.equal(true);
    done();
  });

  it('handles AUTH_LOGOUT', (done) => {
    const initalState = {
      error: '',
      message: '',
      content: '',
      authenticated: true,
    };
    const action = { type: AUTH_LOGOUT };
    const nextState = reducer(initalState, action);
    expect(nextState.authenticated).to.equal(false);
    done();
  });

  it('handles AUTH_ERROR', (done) => {
    const content = 'Some content to carry with.';
    const initalState = {
      error: '',
      message: '',
      content,
      authenticated: true,
    };
    const payload = 'Dummy error message';
    const action = { type: AUTH_ERROR, payload };
    const nextState = reducer(initalState, action);
    expect(nextState.error).to.equal(payload);
    expect(nextState.content).to.equal(content);
    expect(nextState.authenticated).to.equal(true);
    done();
  });
});
