/* eslint-disable no-unused-expressions */
/* global it, describe */
import { expect } from 'chai';
import reducer from '../../../src/shared/redux/reducer/notification';
import { Notification } from '../../../src/shared/helper/Notification';
import {
  NOTIFICATION_ADD,
  NOTIFICATION_UPDATE,
  NOTIFICATION_REMOVE,
  NOTIFICATION_REMOVE_ALL,
  NOTIFICATION_DEFAULT_ACTION,
} from '../../../src/shared/redux/actions/types';

describe('Notification reducer', () => {
  it('handles NOTIFICATION_ADD', (done) => {
    const notification = new Notification(
      'info',
      'Notification title',
      'This is some content.',
      'icon-info',
    );
    const initialState = [];
    const nextState = reducer(initialState, { type: NOTIFICATION_ADD, notification });
    expect(nextState).to.have.lengthOf(1);
    expect(nextState[0]).to.deep.include(notification);
    done();
  });

  it('handles NOTIFICATION_UPDATE', (done) => {
    const notification = new Notification(
      'info',
      'Notification title',
      'This is some content.',
      'icon-info',
    );
    const initialState = [];
    let nextState = reducer(initialState, { type: NOTIFICATION_ADD, notification });
    notification.content = 'This content is updated';
    nextState = reducer(nextState, { type: NOTIFICATION_UPDATE, notification });
    expect(nextState[0]).to.deep.include(notification);
    done();
  });

  it('handles NOTIFICATION_REMOVE', (done) => {
    const notification1 = new Notification(
      'info',
      'Notification title 1',
      'This is some content.',
      'icon-info',
    );
    const notification2 = new Notification(
      'info',
      'Notification title 2',
      'This is some content.',
      'icon-info',
    );
    const notification3 = new Notification(
      'info',
      'Notification title 3',
      'This is some content.',
      'icon-info',
    );
    const initialState = [notification1, notification2, notification3];
    const nextState = reducer(initialState, {
      type: NOTIFICATION_REMOVE,
      notification: notification2,
    });
    expect(nextState).to.have.lengthOf(2);
    expect(nextState[0].id).to.be.equal(notification1.id);
    expect(nextState[1].id).to.be.equal(notification3.id);
    done();
  });

  it('handles NOTIFICATION_REMOVE_ALL', (done) => {
    const initialState = [NOTIFICATION_DEFAULT_ACTION];
    const action = { type: NOTIFICATION_REMOVE_ALL };
    const nextState = reducer(initialState, action);
    expect(nextState).to.be.an('array').that.is.empty;
    done();
  });
});
