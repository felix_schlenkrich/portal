import express from 'express';
import compression from 'compression';
import path from 'path';
import appRootDir from 'app-root-dir';
import logger from '../../utils/logger';
import config from '../../config';
import clientMiddleware from './middleware/client-middleware';
import errorMiddleware from './middleware/error-middleware';

const app = express();

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// TODO: replace morgan with winston
// if (process.env.NODE_ENV == 'dev') app.use(morgan('dev'));

app.disable('x-powered-by');
app.use(compression());

// Serve the client bundle
app.use(config.bundleConfig.client.webPath, clientMiddleware);

// Serve public assets
app.use(express.static(path.join(appRootDir.get(), '../../', config.publicAssetsPath)));

app.get('*', (req, res) => {
  const index = path.resolve(appRootDir.get(), 'public', 'index.html');
  res.status(200).sendFile(index);
});

app.use(...errorMiddleware);

const listener = app.listen(config.network.port, config.network.host, () => {
  logger.info(`Server listening on port ${config.network.port}.`);
});

export default listener;
