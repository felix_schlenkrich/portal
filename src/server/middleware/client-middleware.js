import express from 'express';
import path from 'path';
import appRootDir from 'app-root-dir';
import config from '../../../config';

export default express.static(
  path.resolve(appRootDir.get(), config.bundleConfig.client.outputPath),
  { maxAge: config.browserCacheMaxAge },
);
