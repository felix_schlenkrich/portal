import logger from '../../../utils/logger';

const errorHandlersMiddleware = [
  function notFoundMiddleware(req, res) {
    res.status(404).send('Sorry, the requested resource was not found.');
  },

  function unhandledError(err, req, res) {
    if (err) {
      logger.error(err);
      logger.error(err.stack);
    }
    res.status(500).send('Sorry, an unexpected error occurred');
  },
];

export default errorHandlersMiddleware;
