import {
  NOTIFICATION_ADD,
  NOTIFICATION_UPDATE,
  NOTIFICATION_REMOVE,
  NOTIFICATION_REMOVE_ALL,
} from '../actions/types';

const intialState = [];

export default function (state = intialState, action) {
  switch (action.type) {
    case NOTIFICATION_ADD:
      return [...state, action.notification];
    case NOTIFICATION_UPDATE:
      return state.map((notification) => {
        if (notification.id === action.notification.id) {
          return Object.assign({}, notification, action.notification);
        }
        return notification;
      });
    case NOTIFICATION_REMOVE:
      return state.filter(notification => notification.id !== action.notification.id);
    case NOTIFICATION_REMOVE_ALL:
      return [];
    default:
      return state;
  }
}
