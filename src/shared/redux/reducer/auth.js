import {
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_ERROR,
  AUTH_FORGOT_PASSWORD,
  AUTH_RESET_PASSWORD,
  AUTH_ACTIVATE_EMAIL,
} from '../actions/types';

const initalState = {
  error: '',
  message: '',
  content: '',
  authenticated: false,
  forgotPassword: { message: '', error: null },
  resetPassword: { message: '', error: null },
  activateEmail: { message: '', error: null },
};

export default function (state = initalState, action) {
  switch (action.type) {
    case AUTH_LOGIN:
      return Object.assign({}, state, { error: '', message: '', authenticated: true });
    case AUTH_LOGOUT:
      return Object.assign({}, state, { authenticated: false });
    case AUTH_ERROR:
      return Object.assign({}, state, { ...state, error: action.payload });
    case AUTH_FORGOT_PASSWORD:
      return Object.assign({}, state, {
        ...state,
        forgotPassword: { message: action.message, error: action.error },
      });
    case AUTH_RESET_PASSWORD:
      return Object.assign({}, state, {
        ...state,
        resetPassword: { message: action.message, error: action.error },
      });
    case AUTH_ACTIVATE_EMAIL:
      return Object.assign({}, state, {
        activateEmail: { message: action.message, error: action.error },
      });
    default:
      return state;
  }
}
