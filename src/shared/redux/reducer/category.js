import {
  CATEGORY_ADD,
  CATEGORY_UPDATE,
  CATEGORY_REMOVE,
  CATEGORY_LIST,
  CATEGORY_FIND,
} from '../actions/types';

const intitialState = {};

export default function (state = intitialState, action) {
  switch (action.type) {
    case CATEGORY_ADD:
    case CATEGORY_UPDATE:
    case CATEGORY_REMOVE:
    case CATEGORY_LIST:
    case CATEGORY_FIND:
    default:
      return state;
  }
}
