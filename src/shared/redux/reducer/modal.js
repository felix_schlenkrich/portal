import { MODAL_SHOW, MODAL_HIDE } from '../actions/types';

const intitialState = {
  type: null,
  props: {},
};

export default function (state = intitialState, action) {
  switch (action.type) {
    case MODAL_SHOW:
      return Object.assign({}, state, {
        type: action.payload.type,
        props: action.payload.props,
      });
    case MODAL_HIDE:
      return intitialState;
    default:
      return state;
  }
}
