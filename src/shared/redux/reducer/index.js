import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';
import { reducer as formReducer } from 'redux-form';
import auth from './auth';
import notification from './notification';
import content from './content';
import modal from './modal';

export default combineReducers({
  auth,
  notification,
  content,
  modal,
  form: formReducer,
  router: routerStateReducer,
});
