import {
  CONTENT_ADD,
  CONTENT_UPDATE,
  CONTENT_REMOVE,
  CONTENT_LIST,
  CONTENT_GET,
  CONTENT_RESET,
} from '../actions/types';

const intitialState = {};

export default function (state = intitialState, action) {
  switch (action.type) {
    case CONTENT_ADD:
      return state;
    case CONTENT_UPDATE:
      return state;
    case CONTENT_REMOVE:
      return Object.assign({}, state, {
        list: state.list.filter(item => item._id !== action.payload._id),
      });
    case CONTENT_LIST:
      return Object.assign({}, state, { list: action.list });
    case CONTENT_GET:
      return Object.assign({}, state, { item: action.payload.content });
    case CONTENT_RESET:
      return {};
    default:
      return state;
  }
}
