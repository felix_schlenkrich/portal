import { TAG_ADD, TAG_UPDATE, TAG_REMOVE, TAG_LIST, TAG_FIND } from '../actions/types';

const intitialState = {};

export default function (state = intitialState, action) {
  switch (action.type) {
    case TAG_ADD:
    case TAG_UPDATE:
    case TAG_REMOVE:
    case TAG_LIST:
    case TAG_FIND:
    default:
      return state;
  }
}
