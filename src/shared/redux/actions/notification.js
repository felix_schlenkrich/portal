import { NOTIFICATION_ADD, NOTIFICATION_REMOVE } from './types';

export const add = (action, dispatch) => {
  setTimeout(() => {
    dispatch({
      type: NOTIFICATION_REMOVE,
      notification: action.notification,
    });
  }, 4000);
  dispatch({
    type: NOTIFICATION_ADD,
    notification: action.notification,
  });
};

export const update = (state, action) =>
  state.map((notification) => {
    if (notification.id === action.notification.id) {
      return Object.assign({}, notification, action.notification);
    }
    return notification;
  });

export const remove = (state, action) =>
  state.filter(notification => notification.id !== action.notification.id);

export const removeAll = () => [];
