import axios from 'axios';
import cookie from 'js-cookie';
import {
  CONTENT_ADD,
  CONTENT_UPDATE,
  CONTENT_LIST,
  CONTENT_REMOVE,
  CONTENT_GET,
  CONTENT_RESET,
  NOTIFICATION_ADD,
} from './types';
import { Notification } from '../../helper/Notification';
import { add as addNotification } from './notification';

const API_URL = 'http://localhost:5000/api';
const CLIENT_ROOT_URL = 'http://localhost:3000';

axios.defaults.headers.common.Authorization = `${cookie.get('token')}`;

export const add = (title, body) => (dispatch) => {
  axios
    .post(`${API_URL}/content/create`, { title, body })
    .then((response) => {
      dispatch({ type: CONTENT_ADD });
      window.location.href = `${CLIENT_ROOT_URL}/dashboard`;
    })
    .catch((error) => {
      // do error handling here
    });
};
export const update = (_id, title, body) => (dispatch) => {
  axios
    .post(`${API_URL}/content/update`, { content: { _id, title, body } })
    .then((response) => {
      const notification = new Notification(
        'success',
        'Update',
        'The item was successfully updated.',
        'fa-info-circle',
      );
      dispatch({
        type: CONTENT_UPDATE,
        payload: { content: response.data.content },
      });
      addNotification(
        {
          type: NOTIFICATION_ADD,
          notification,
        },
        dispatch,
      );
    })
    .catch((error) => {
      console.log('error', error);
      // do error handling
    });
};
export const remove = _id => (dispatch) => {
  axios
    .post(`${API_URL}/content/delete`, { _id })
    .then((response) => {
      dispatch({
        type: CONTENT_REMOVE,
        payload: { _id },
      });
    })
    .catch((error) => {
      // do error handling
    });
};
export const list = () => (dispatch) => {
  axios
    .post(`${API_URL}/content/list`)
    .then((response) => {
      dispatch({ type: CONTENT_LIST, list: response.data });
    })
    .catch((error) => {
      // do error handling here
    });
};
export const get = _id => (dispatch) => {
  axios
    .post(`${API_URL}/content/get`, { _id })
    .then((response) => {
      dispatch({
        type: CONTENT_GET,
        payload: { content: response.data },
      });
    })
    .catch((error) => {
      // error handling
    });
};

export const reset = _id => (dispatch) => {
  dispatch({ type: CONTENT_RESET });
};
