// Authentification
export const AUTH_LOGIN = 'auth-login';
export const AUTH_LOGOUT = 'auth-logout';
export const AUTH_ERROR = 'auth-error';
export const AUTH_FORGOT_PASSWORD = 'auth-forgot-password';
export const AUTH_RESET_PASSWORD = 'auth-reset-password';
export const AUTH_ACTIVATE_EMAIL = 'auth-activate-email';
export const AUTH_PROTECTED_TEST = 'auth-protected-request';

// Notifications
export const NOTIFICATION_ADD = 'notification-add';
export const NOTIFICATION_UPDATE = 'notification-update';
export const NOTIFICATION_REMOVE = 'notification-remove';
export const NOTIFICATION_REMOVE_ALL = 'notification-remove-all';

export const NOTIFICATION_DEFAULT_ACTION = {
  id: null,
  level: 'info',
  title: null,
  content: null,
  icon: null,
};

// Content
export const CONTENT_ADD = 'content-add';
export const CONTENT_UPDATE = 'content-update';
export const CONTENT_REMOVE = 'content-remove';
export const CONTENT_LIST = 'content-list';
export const CONTENT_GET = 'content-get';
export const CONTENT_RESET = 'content-reset';

// Tags
export const TAG_ADD = 'tag-add';
export const TAG_UPDATE = 'tag-update';
export const TAG_REMOVE = 'tag-remove';
export const TAG_LIST = 'tag-list';
export const TAG_FIND = 'tag-find';

// Categories
export const CATEGORY_ADD = 'category-add';
export const CATEGORY_UPDATE = 'category-update';
export const CATEGORY_REMOVE = 'category-remove';
export const CATEGORY_LIST = 'category-list';
export const CATEGORY_FIND = 'category-find';

// Modal
export const MODAL_SHOW = 'modal-show';
export const MODAL_HIDE = 'modal-hide';
