import axios from 'axios';
import cookie from 'js-cookie';
import {
  AUTH_LOGIN,
  AUTH_ERROR,
  AUTH_LOGOUT,
  AUTH_FORGOT_PASSWORD,
  AUTH_RESET_PASSWORD,
  AUTH_ACTIVATE_EMAIL,
} from './types';

const API_URL = 'http://localhost:5000/api';
const CLIENT_ROOT_URL = 'http://localhost:3000';

export function logoutUser() {
  return (dispatch) => {
    cookie.remove('token', { path: '/' });
    window.location.href = `${CLIENT_ROOT_URL}/login`;
    dispatch({ type: AUTH_LOGOUT });
  };
}

export function errorHandler(dispatch, error, type) {
  let errorMessage = '';

  if (error.data) {
    errorMessage = error.data;
  } else if (error.response) {
    errorMessage = error.response.data.message;
  } else {
    errorMessage = error;
  }

  if (error.status === 401) {
    dispatch({
      type,
      message: 'You are not authorized to do this. Please login and try again.',
    });
    logoutUser();
  } else {
    dispatch({
      type,
      message: errorMessage,
    });
  }
}

export function loginUser(email, password) {
  return (dispatch) => {
    axios
      .post(`${API_URL}/auth/login`, { email, password })
      .then((response) => {
        cookie.set('token', response.data.token, { path: '/' });
        dispatch({ type: AUTH_LOGIN });
        window.location.href = `${CLIENT_ROOT_URL}/dashboard`;
      })
      .catch((error) => {
        errorHandler(dispatch, error, AUTH_ERROR);
      });
  };
}

export function signupUser(email, username, password, newsletter) {
  return (dispatch) => {
    axios
      .post(`${API_URL}/auth/signup`, { email, username, password, newsletter })
      .then((response) => {
        cookie.set('token', response.data.token, { path: '/' });
        dispatch({ type: AUTH_LOGIN });
        window.location.href = `${CLIENT_ROOT_URL}/dashboard`;
      })
      .catch((error) => {
        errorHandler(dispatch, error, AUTH_ERROR);
      });
  };
}

export function forgotPassword(email) {
  return (dispatch) => {
    axios
      .post(`${API_URL}/auth/forgot-password`, { email })
      .then((response) => {
        dispatch({ type: AUTH_FORGOT_PASSWORD, message: response.data.message });
      })
      .catch((error) => {
        errorHandler(dispatch, error, AUTH_FORGOT_PASSWORD);
      });
  };
}

export function resetPassword(token, password) {
  return (dispatch) => {
    axios
      .post(`${API_URL}/auth/reset-password`, { token, password })
      .then((response) => {
        dispatch({ type: AUTH_RESET_PASSWORD, message: response.data.message });
      })
      .catch((error) => {
        errorHandler(dispatch, error, AUTH_RESET_PASSWORD);
      });
  };
}

export function activateEmail(token) {
  return (dispatch) => {
    axios
      .post(`${API_URL}/auth/activate-email`, { token })
      .then((response) => {
        dispatch({ type: AUTH_ACTIVATE_EMAIL, message: response.data.message });
      })
      .catch((error) => {
        errorHandler(dispatch, error, AUTH_ACTIVATE_EMAIL);
      });
  };
}
