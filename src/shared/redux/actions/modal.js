import { MODAL_SHOW, MODAL_HIDE } from './types';

export const showModal = (type, props) => ({
  type: MODAL_SHOW,
  payload: {
    type,
    props,
  },
});

export const hideModal = () => ({
  type: MODAL_HIDE,
});
