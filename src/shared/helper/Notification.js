import uuid from 'uuid';

export class Notification {
  constructor(level, title, content, icon) {
    this.id = uuid.v4();
    this.level = level;
    this.title = title;
    this.content = content;
    this.icon = icon;
  }

  toString() {
    return `${this.level} - ${this.title}: ${this.content}`;
  }
}
