import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ContentSchema = Schema(
  {
    title: { type: String, required: true },
    body: { type: String },
    createdBy: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
    categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
    urlName: { type: String, required: false }, // TODO: Add required: true
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Content', ContentSchema);
