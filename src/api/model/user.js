import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      unique: true,
      required: true,
    },
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    profile: {
      firstName: { type: String },
      lastName: { type: String },
    },
    role: {
      type: String,
      enum: ['Member', 'Client', 'Owner', 'Admin'],
      default: 'Member',
    },
    newsletter: { type: Boolean },
    emailActivated: { type: Boolean },
    emailActivationToken: { type: String },
    emailActivationTokenExpires: { type: Date },
    resetPasswordToken: { type: String },
    resetPasswordExpires: { type: Date },
  },
  {
    timestamps: true,
  },
);

UserSchema.pre('save', function preSave(next) {
  const user = this;
  const SALT_FACTOR = 5;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_FACTOR, (error, salt) => {
    if (error) return next(error);

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(error);
      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (error, isMatch) => {
    if (error) return cb(error);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);
