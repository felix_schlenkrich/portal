import mongoose from 'mongoose';

const TagSchema = mongoose.Schema(
  {
    title: { type: String, required: true },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Tag', TagSchema);
