import express from 'express';
import mongoose from 'mongoose';
import bluebird from 'bluebird';
import bodyParser from 'body-parser';
import router from './router';
import config from '../../config';
import logger from '../../utils/logger';

const app = express();

// TODO: Replace logger with winston
// app.use(morgan('dev'));
mongoose.Promise = bluebird;
// eslint-disable-next-line no-unused-vars
mongoose.connect(config.db.connectionString, { useMongoClient: true }).then((db) => {});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Enable CORS from client-side
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
  );
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
});

router(app);
const listener = app.listen(config.network.portAPI, () =>
  logger.info(`Portal API listening on port ${config.network.portAPI}`),
);
export default listener;
