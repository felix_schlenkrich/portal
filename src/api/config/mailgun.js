import mailgunJs from 'mailgun-js';
import config from '../../../config';
import logger from '../../../utils/logger';

const mailgun = mailgunJs({
  apiKey: config.mailgun.apiKey,
  domain: config.mailgun.domain,
});

export function sendMail(message) {
  const mail = {
    from: config.mailgun.from,
    to: message.recipient,
    subject: message.subject,
    text: message.text,
  };

  mailgun.messages().send(mail, (error, body) => {
    if (!error) {
      logger.info(`Message was sent to ${mail.to}`);
      logger.info(body);
    } else {
      logger.info('error', error);
    }
  });
}
