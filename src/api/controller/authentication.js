import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import User from '../model/user';
import { sendMail } from '../config/mailgun';
import config from '../../../config';
import logger from '../../../utils/logger';
import { OK, CREATED, BAD_REQUEST, UNAUTHORIZED } from '../../../utils/httpStatusCodes';

function generateToken(user) {
  return jwt.sign(user, config.auth.secret, {
    expiresIn: 10080, // seconds
  });
}

function setUserInfo(request) {
  return {
    _id: request._id,
    username: request.username,
    email: request.email,
    role: request.role,
  };
}

export function login(req, res, next) {
  User.findOne({ $or: [{ email: req.body.email }, { username: req.body.email }] })
    .exec()
    .then((user) => {
      res.status(OK).json({
        token: `Bearer ${generateToken({ email: user.email })}`,
        user: { email: user.email },
      });
    })
    .catch((error) => {
      res.status(BAD_REQUEST).json({
        message: 'Your request could not be processed',
        error,
      });
      return next(error);
    });
}

export function register(req, res, next) {
  const email = req.body.email;
  const username = req.body.username;
  const password = req.body.password;
  const newsletter = req.body.newsletter; // optional

  if (!email) {
    return res.status(BAD_REQUEST).send({
      error: 'You must enter your full email address.',
    });
  }

  if (!username) {
    return res.status(BAD_REQUEST).send({
      error: 'You must enter your full name.',
    });
  }

  if (!password) {
    return res.status(BAD_REQUEST).send({
      error: 'You must enter a password.',
    });
  }

  User.findOne({
    email,
  })
    .exec()
    .then((exisitingUser) => {
      if (exisitingUser) {
        return res.status(BAD_REQUEST).send({
          error: 'The email address is already in use.',
        });
      }

      const user = new User({
        email,
        username,
        password,
        newsletter,
      });

      return user.save();
    })
    .then((user) => {
      const userInfo = setUserInfo(user);

      // Activation mail
      crypto.randomBytes(48, (error, buffer) => {
        if (error) return next(error);
        const resetToken = buffer.toString('hex');
        user.emailActivationToken = resetToken;
        user.emailActivationTokenExpires = Date.now() + 604800000;

        user.save((err, user) => {
          sendMail({
            recipient: user.email,
            subject: 'Activate your account',
            text:
              `${'You are receiving this because you (or someone else) have registered ac account on //TODO Sitename.\n\n' +
                'Please click on the following link, or paste this into your browser to activate your account:\n\n'}${config.network.getClientUrl()}/activate/${user.emailActivationToken}\n\n` +
              'If you did not register on our site yourself, please inform us.\n',
          });
          res.status(CREATED).json({
            token: `Bearer ${generateToken(userInfo)}`,
            user: userInfo,
            error: false,
            message: 'Account was registered successfull.',
          });
          return next();
        });
      });
    })
    .catch((err) => {
      if (err) return next(err);
    });
}

export function userExists(req, res, next) {
  logger.info('userExists');
  User.findOne({
    username: req.body.username,
  })
    .exec()
    .then((user) => {
      if (user) {
        res.status(OK).json({
          taken: true,
        });
      } else {
        res.status(OK).json({
          taken: false,
        });
      }
    })
    .catch((error) => {
      res.status(BAD_REQUEST).json({
        message: 'Your request could not be processed',
        error,
      });
      next(error);
    });
}

export function emailExists(req, res, next) {
  logger.info('emailExists');
  User.findOne({
    email: req.body.email,
  })
    .exec()
    .then((user) => {
      if (user) {
        res.status(OK).json({
          taken: true,
        });
      } else {
        res.status(OK).json({
          taken: false,
        });
      }
    })
    .catch((error) => {
      res.status(BAD_REQUEST).json({
        message: 'Your request could not be processed',
        error,
      });
      next(error);
    });
}

export function forgotPassword(req, res, next) {
  logger.info('forgotPassword', req.body.email);
  const email = req.body.email;
  User.findOne({
    email,
  })
    .exec()
    .then((user) => {
      crypto.randomBytes(48, (error, buffer) => {
        if (error) return next(error);
        const resetToken = buffer.toString('hex');
        user.resetPasswordToken = resetToken;
        user.resetPasswordExpires = Date.now() + 3600000;

        user.save((err, user) => {
          sendMail({
            recipient: user.email,
            subject: 'Reset password',
            text:
              `${'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please click on the following link, or paste this into your browser to complete the process:\n\n'}${config.network.getClientUrl()}/reset-password/${user.resetPasswordToken}\n\n` +
              'If you did not request this, please ignore this email and your password will remain unchanged.\n',
          });
          res.status(OK).json({
            error: false,
            message: 'An E-Mail with reset link was sent.',
          });
          return next();
        });
      });
    })
    .catch((error) => {
      res.status(BAD_REQUEST).json({
        message: 'Your request could not be processed',
        error,
      });
      return next(error);
    });
}

export function resetPassword(req, res, next) {
  User.findOne({
    resetPasswordToken: req.body.token,
    resetPasswordExpires: {
      $gt: Date.now(),
    },
  })
    .exec()
    .then((user) => {
      user.password = req.body.password;
      user.resetPasswordToken = undefined;
      user.resetPasswordExpires = undefined;

      user.save((err, user) => {
        sendMail({
          recipient: user.email,
          subject: 'Password changed',
          text:
            'You are receiving this email because you changed your password. \n\n' +
            'If you did not request this change, please contact us immediately.',
        });
        res.status(OK).json({
          message: 'Password changed successfully. Please login with your new password.',
        });
        return next();
      });
    })
    .catch((error) => {
      res.status(BAD_REQUEST).json({
        message: 'Your request could not be processed',
        error,
      });
      return next(error);
    });
}

export function activateEmail(req, res, next) {
  User.findOne({
    emailActivationToken: req.body.token,
    emailActivationTokenExpires: {
      $gt: Date.now(),
    },
  })
    .exec()
    .then((user) => {
      if (!user) {
        res.status(BAD_REQUEST).json({
          message: 'Your request could not be processed',
          error: true,
        });
        return next();
      }

      user.emailActivated = true;
      user.emailActivationToken = undefined;
      user.emailActivationTokenExpires = undefined;

      user.save((err, user) => {
        sendMail({
          recipient: user.email,
          subject: 'Account activated',
          text: 'Your account is activated successfully. \n\n',
        });
        res.status(OK).json({
          message: 'Account successfully activated.',
        });
        return next();
      });
    })
    .catch((error) => {
      res.status(BAD_REQUEST).json({
        message: 'Your request could not be processed',
        error,
      });
      return next(error);
    });
}

export function roleAuthorization(role) {
  return (req, res, next) => {
    const user = req.user;

    User.findById(user._id)
      .exec()
      .then((foundUser) => {
        if (foundUser.role === role) return next();

        res.send(UNAUTHORIZED).json({
          error: 'You are not authorized to view this content.',
        });
        return next('Unauthorized');
      })
      .catch((err) => {
        if (err) return next(err);
      });
  };
}
