import Content from '../model/content';

export const createContent = (req, res, next) => {
  const title = req.body.title;
  const body = req.body.body;

  const newContent = new Content({
    title,
    body,
    createdBy: req.user._id,
  });

  newContent
    .save()
    .then((content) => {
      res.status(201).json({
        content,
      });
    })
    .catch(error => next(error));
};

export const listContent = (req, res, next) => {
  Content.find()
    .exec()
    .then((content) => {
      res.status(201).json(content);
    })
    .catch(error => next(error));
};

export const updateContent = (req, res, next) => {
  Content.update({ _id: req.body.content._id }, req.body.content)
    .then((content) => {
      res.status(201).json(content);
    })
    .catch(error => next(error));
};

export const deleteContent = (req, res, next) => {
  Content.remove({ _id: req.body._id })
    .then(res.status(201).json({ error: false }))
    .catch(error => next(error));
};

export const getContent = (req, res, next) => {
  Content.findOne({ _id: req.body._id })
    .then(content => res.status(201).json(content))
    .catch(error => next(error));
};
