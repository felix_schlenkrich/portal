import express from 'express';
import passport from 'passport';
import {
  login,
  register,
  userExists,
  emailExists,
  activateEmail,
  resetPassword,
  forgotPassword,
} from './controller/authentication';
import { createContent, updateContent, listContent, deleteContent, getContent } from './controller/content';
// eslint-disable-next-line no-unused-vars
import passportService from './config/passport';

// eslint-disable-next-line no-unused-vars
const requireAuth = passport.authenticate('jwt', { session: false });
const requireLogin = passport.authenticate('local', { session: false });

// eslint-disable-next-line no-unused-vars
const REQUIRE_ADMIN = 'Admin';
// eslint-disable-next-line no-unused-vars
const REQUIRE_OWNER = 'Owner';
// eslint-disable-next-line no-unused-vars
const REQUIRE_CLIENT = 'Client';
// eslint-disable-next-line no-unused-vars
const REQUIRE_MEMBER = 'Member';

function router(app) {
  const apiRoutes = express.Router();
  const baseRoutes = express.Router();
  const authRoutes = express.Router();
  const contentRoutes = express.Router();

  apiRoutes.use((req, res, next) => {
    // do logging
    next(); // make sure we go to the next routes and don't stop here
  });

  apiRoutes.use('/base', baseRoutes);

  // Set auth routes as subgroup/middleware to apiRoutes
  apiRoutes.use('/auth', authRoutes);

  apiRoutes.use('/content', contentRoutes);

  authRoutes.post('/signup', register);
  authRoutes.post('/login', requireLogin, login);
  authRoutes.post('/user-exists', userExists);
  authRoutes.post('/email-exists', emailExists);
  authRoutes.post('/forgot-password', forgotPassword);
  authRoutes.post('/reset-password', resetPassword);
  authRoutes.post('/activate-email', activateEmail);

  contentRoutes.post('/create', requireAuth, createContent);
  contentRoutes.post('/list', requireAuth, listContent);
  contentRoutes.post('/update', requireAuth, updateContent);
  contentRoutes.post('/delete', requireAuth, deleteContent);
  contentRoutes.post('/get', requireAuth, getContent);

  baseRoutes.post('/ping', (req, res) => {
    res.status(200).json({ alive: true });
  });

  // Set url for API group routes
  app.use('/api', apiRoutes);
}

export default router;
