import React from 'react';

export function renderInput(field) {
  const classes = ['form-control'];
  if (field.meta.touched && field.meta.error) classes.push('is-invalid');
  else if (field.meta.touched) classes.push('is-valid');
  return (
    <div className="form-group">
      <label htmlFor={field.name}>
        {field.label}
      </label>
      <input
        {...field.input}
        type={field.type}
        className={classes.join(' ')}
        placeholder={field.placeholder}
      />
      {field.meta.touched &&
        field.meta.error &&
        <div className="invalid-feedback">
          {field.meta.error}
        </div>}
    </div>
  );
}

export function renderTextarea(field) {
  const classes = ['form-control'];
  if (field.meta.touched && field.meta.error) classes.push('is-invalid');
  else if (field.meta.touched) classes.push('is-valid');

  const describedBy = `${field.name}Help`;
  return (
    <div className="form-group">
      <label htmlFor={field.name}>
        {field.label}
      </label>
      <textarea {...field.input} className={classes.join(' ')} placeholder={field.placeholder} />
      {field.meta.touched &&
        field.meta.error &&
        <div className="invalid-feedback">
          {field.meta.error}
        </div>}
    </div>
  );
}

export function renderCheckbox(field) {
  const classes = ['form-check-input', 'mr-2'];
  if (field.meta.touched && field.meta.error) classes.push('is-invalid');
  else if (field.meta.touched) classes.push('is-valid');

  const describedBy = `${field.name}Help`;
  return (
    <div className="form-check">
      <label className="form-check-label" htmlFor={field.name}>
        <input
          {...field.input}
          type="checkbox"
          className={classes.join(' ')}
          placeholder={field.placeholder}
          aria-describedby={describedBy}
        />
        {field.label}
      </label>
      {field.meta.touched &&
        field.meta.error &&
        <div className="invalid-feedback">
          {field.meta.error}
        </div>}
    </div>
  );
}
