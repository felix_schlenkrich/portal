import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; 
import { Link } from 'react-router';

const card = (content, props) => (
  <div className="card mb-3" key={content._id}>
    <div className="card-body">
      <h5 className="card-title">{content.title}</h5>
      <small>{content.createdAt}</small>
      <div className="card-text text-truncate mb-3">{content.body}</div>
      <div>
        <Link to={`/content/edit/${content._id}`} className="btn btn-primary btn-sm mr-3">
          <i className="fa fa-pencil mr-2" aria-hidden="true" />Edit
        </Link>
        <button to={'/content/delete'} onClick={props.removeContent.bind(null, content._id)} className="btn btn-secondary btn-sm mr-3">
          <i className="fa fa-trash-o mr-2" aria-hidden="true" />Delete
        </button>
      </div>
    </div>
  </div>
);

const Dashboard = (props) => {
  const cards = [];

  if (typeof props.list !== 'undefined') {
    props.list.forEach((content) => {
      cards.push(card(content, props));
    });
  }

  return (
    <div>
      <h1 className="mb-3">Dashboard</h1>
      <div className="row mb-3">
        <div className="col-auto mr-auto">
          <h2>Your content</h2>
        </div>
        <div className="col-auto">
          <Link to={'/content/create'} className="btn btn-outline-dark">
            Create
          </Link>
        </div>
      </div>
      <ReactCSSTransitionGroup
        transitionName="content"
        transitionEnterTimeout={400}
        transitionLeaveTimeout={400}
      >
        {cards}
      </ReactCSSTransitionGroup>
    </div>
  );
};

export default Dashboard;
