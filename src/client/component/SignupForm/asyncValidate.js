import axios from 'axios';

const API_URL = 'http://localhost:5000/api';

const asyncValidate = data =>
  // if (!data.username) return Promise.resolve();

  new Promise((resolve, reject) => {
    const errors = {};

    axios
      .post(`${API_URL}/auth/user-exists`, { username: data.username })
      .then((response) => {
        if (response.data.taken) errors.username = 'This username is already taken.';
        return axios.post(`${API_URL}/auth/email-exists`, { email: data.email });
      })
      .then((response) => {
        if (response.data.taken) errors.email = 'This e-mail address is already in use.';

        if (
          Object.prototype.hasOwnProperty.call(errors, 'username') ||
          Object.prototype.hasOwnProperty.call(errors, 'email')
        ) {
          reject(errors);
        } else resolve();
      })
      .catch((error) => {
        // do error handling here
      });
  })
;

export default asyncValidate;
