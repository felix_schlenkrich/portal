import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import asyncValidate from './asyncValidate';
import { renderInput, renderCheckbox } from '../../helper/fields';

const SignupForm = (props) => {
  const { handleSubmit } = props;
  return (
    <div className="row justify-content-md-center">
      <div className="col-md-6">
        <h1 className="mt-3">Sign Up</h1>
        <form onSubmit={handleSubmit(props.onSubmit)} className="mt-3">
          <Field
            type="text"
            component={renderInput}
            name="username"
            placeholder="Enter a username"
            label="Username"
          />
          <Field
            type="email"
            component={renderInput}
            name="email"
            placeholder="Enter email"
            label="E-Mail"
          />
          <Field
            type="password"
            component={renderInput}
            name="password"
            placeholder="Enter password"
            label="Password"
          />
          <Field
            type="password"
            component={renderInput}
            name="cpassword"
            placeholder="Repeat password"
            label="Confirm password"
          />
          <Field
            type="checkbox"
            component={renderCheckbox}
            name="newsletter"
            label="Yes, keep me up to date about Fastereders Headquarters"
          />
          <hr />
          <small>
            By clicking on <u>Signup</u> below, you are agreeing to the{' '}
            <a href="#">Terms of Service</a> and the <a href="#">Privacy Policy</a>.
          </small>
          <hr />
          <button type="submit" className="btn btn-outline-dark">
            Sign up
          </button>
        </form>
      </div>
    </div>
  );
};

export default reduxForm({
  form: 'form-signup',
  validate,
  asyncValidate,
})(SignupForm);
