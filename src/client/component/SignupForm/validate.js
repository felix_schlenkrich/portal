import isEmail from 'validator/lib/isEmail';

export default function validate(formProps) {
  const errors = {};
  if (!formProps.username) errors.username = 'Please enter an username.';
  if (formProps.password && formProps.password.length < 8) {
    errors.password = 'Password must have at least 8 characters.';
  }
  if (!formProps.email) errors.email = 'Please enter e email address.';
  if (formProps.email && !isEmail(formProps.email)) {
    errors.email = 'Please enter a valid email address.';
  }
  if (!formProps.password) errors.password = 'Please enter a password.';
  if (!formProps.cpassword) errors.cpassword = 'Please comfirm the password.';
  if (formProps.password !== formProps.cpassword) {
    errors.cpassword = 'Please confirm with the correct password.';
  }
  return errors;
}
