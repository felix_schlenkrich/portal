import React from 'react';
import { Link } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import { renderInput } from '../../helper/fields';

const LoginForm = (props) => {
  const { handleSubmit } = props;

  return (
    <div className="row justify-content-md-center">
      <div className="col-md-6">
        <h1 className="mt-3">Login</h1>
        <form onSubmit={handleSubmit(props.onSubmit)} className="mt-3">
          <Field
            component={renderInput}
            type="text"
            name="email"
            placeholder="Enter your email or username."
            label="E-Mail / Username"
          />

          <Field
            component={renderInput}
            type="password"
            name="password"
            placeholder="Enter your password"
            label="Password"
          />

          <div className="d-flex justify-content-between p-2">
            <div>
              <small>
                New to sitename? <a href="#">Create an account.</a>
              </small>
            </div>
            <div>
              <small>
                <Link to={'/forgot-password'}>Forgot password?</Link>
              </small>
            </div>
          </div>

          <hr />

          <button type="submit" className="btn btn-outline-dark mt-2">
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default reduxForm({
  form: 'form-login',
  validate,
})(LoginForm);
