const validate = (formProps) => {
  const errors = {};

  if (!formProps.title) errors.title = 'Please enter a title';
  if (!formProps.body) errors.body = 'Please enter some content';

  return errors;
};

export default validate;
