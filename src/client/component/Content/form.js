import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import { renderInput, renderTextarea } from '../../helper/fields';

const ContentForm = (props) => {
  const { handleSubmit } = props;
  return (
    <div className="row justify-content-md-center">
      <div className="col-md-6">
        <h1 className="mt-3">Content</h1>
        <form onSubmit={handleSubmit(props.onSubmit)} id="form-content">
          <Field
            component={renderInput}
            type="text"
            name="title"
            placeholder="Enter a title"
            label="Title"
            description="Add some awesome title here."
          />
          <Field
            component={renderTextarea}
            name="body"
            placeholder="Start typing ..."
            label="Body"
            description="Put your content here."
          />
          <button type="submit" className="btn btn-primary mt-2">
            Save
          </button>
        </form>
      </div>
    </div>
  );
};

export default reduxForm({
  form: 'form-content',
  validate,
  enableReinitialize: true,
})(ContentForm);
