import React from 'react';
import { Link } from 'react-router';

const dummyText =
  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';

const Home = () => (
  <div>
    <div className="jumbotron bg-dark text-light">
      <h3 className="display-4">Welcome buddy</h3>
      <p className="lead">Fastereders International Headquarters - Your best choice.</p>
      <hr className="my-4 bg-light" />
      <p className="lead">
        <Link className="btn btn-outline-light btn-lg" to={'/signup'}>
          Join now
        </Link>
      </p>
    </div>
    <p>{dummyText}</p>
    <p>{dummyText}</p>
    <p>{dummyText}</p>
    <p>{dummyText}</p>
    <p>{dummyText}</p>
  </div>
);

export default Home;
