import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { AUTH_LOGOUT } from '../../../shared/redux/actions/types';
import { logoutUser } from '../../../shared/redux/actions/authentication';

class Navigation extends Component {
  onLogout() {
    logoutUser();
    this.props.onLogout();
  }

  render() {
    let login = null;
    let logout = null;
    let signup = null;
    let dashboard = null;

    const logo = (
      <Link className="mr-3 text-light" to={'/'} activeClassName="active">
        <i className="fa fa-hand-rock-o" aria-hidden="true" />
        <span className="hidden-xs-down" />
      </Link>
    );

    const start = (
      <Link className="mr-3 text-light" to={'/'} activeClassName="active">
        Start <span className="sr-only">(current)</span>
      </Link>
    );

    const about = (
      <Link className="mr-3 text-light" to={'/about'} activeClassName="active">
        About
      </Link>
    );

    if (!this.props.authenticated) {
      login = (
        <Link className="mr-3 text-light" to={'/login'} activeClassName="active">
          Login
        </Link>
      );
      signup = (
        <Link className="mr-3 text-light" to={'/signup'} activeClassName="active">
          Signup
        </Link>
      );
    } else {
      logout = (
        <a className="mr-3 pointer text-light" onClick={this.onLogout.bind(this)}>
          Logout
        </a>
      );
      dashboard = (
        <Link className="mr-3 text-light" to={'/dashboard'} activeClassName="active">
          Dashboard
        </Link>
      );
    }

    return (
      <nav className="d-flex flex-row bg-dark text-white p-2 mb-3 sticky-top">
        <div className="container">
          <div className="d-flex flex-row ">
            {logo}
            {start}
            {about}
            {dashboard}
            {signup}
            {login}
            {logout}
          </div>
        </div>
      </nav>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch({ type: AUTH_LOGOUT }),
});

const mapStateToProps = state => ({ authenticated: state.auth.authenticated });

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
