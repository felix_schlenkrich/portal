export default function validate(form) {
  const errors = {};
  if (!form.email) errors.email = 'Please enter your e-mail address.';
  return errors;
}
