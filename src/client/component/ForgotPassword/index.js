import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import { renderInput } from '../../helper/fields';

const ForgotPasswordForm = (props) => {
  const { handleSubmit } = props;

  return (
    <div className="row justify-content-md-center">
      <div className="col-md-6">
        <h1 className="mt-3">Forgot password</h1>
        <form onSubmit={handleSubmit(props.onSubmit)} className="mt-3">
          <Field
            component={renderInput}
            type="text"
            name="email"
            placeholder="Enter your email."
            label="E-Mail"
          />

          <hr />

          <button type="submit" className="btn btn-outline-dark mt-2">
            Request new password
          </button>
        </form>
      </div>
    </div>
  );
};

export default reduxForm({
  form: 'form-forgot-password',
  validate,
})(ForgotPasswordForm);
