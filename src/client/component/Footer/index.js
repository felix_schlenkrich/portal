import React from 'react';

const Footer = () =>
  (<div className="bg-dark mt-5">
    <div className="container">
      <div className="row pb-5 pt-5">
        <div className="col">
          <ul className="list-unstyled text-light">
            <li>Far far away</li>
            <li>Far far away</li>
            <li>Far far away</li>
          </ul>
        </div>
        <div className="col">
          <ul className="list-unstyled text-light">
            <li>A small river namedy</li>
            <li>A small river named</li>
            <li>A small river named</li>
          </ul>
        </div>
        <div className="col">
          <ul className="list-unstyled text-light">
            <li>Far far away</li>
            <li>Far far away</li>
            <li>Far far away</li>
          </ul>
        </div>
      </div>
    </div>
  </div>);

export default Footer;
