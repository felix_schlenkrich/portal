import React from 'react';

const Activate = props =>
  (<div className="row justify-content-md-center">
    <div className="col-md-6">
      <h1 className="mt-3">Activation</h1>
      <div>
        {props.message}
      </div>
    </div>
  </div>);

export default Activate;
