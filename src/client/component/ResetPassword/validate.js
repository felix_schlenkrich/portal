export default function validate(formProps) {
  const errors = {};
  if (!formProps.password) errors.password = 'Please enter a password.';
  if (!formProps.cpassword) errors.cpassword = 'Please comfirm the password.';
  if (formProps.password !== formProps.cpassword) {
    errors.cpassword = 'Please confirm with the correct password.';
  }
  return errors;
}
