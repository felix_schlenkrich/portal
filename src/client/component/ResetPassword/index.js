import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import { renderInput } from '../../helper/fields';

const ResetPasswordForm = (props) => {
  const { handleSubmit } = props;

  return (
    <div className="row justify-content-md-center">
      <div className="col-md-6">
        <h1 className="mt-3">Reset password</h1>
        <form onSubmit={handleSubmit(props.onSubmit)} className="mt-3">
          <Field
            component={renderInput}
            type="password"
            name="password"
            placeholder="Enter your password"
            label="Password"
          />

          <Field
            component={renderInput}
            type="password"
            name="cpassword"
            placeholder="Confirm your password"
            label="Comfirm password"
          />

          <button type="submit" className="btn btn-outline-dark mt-2">
            Reset password
          </button>
        </form>
      </div>
    </div>
  );
};

export default reduxForm({
  form: 'form-reset-password',
  validate,
})(ResetPasswordForm);
