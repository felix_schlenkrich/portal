import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const Notification = props => (
  <div id="notification-container">
    <ReactCSSTransitionGroup
      transitionName="notification"
      transitionEnterTimeout={300}
      transitionLeaveTimeout={300}
    >
      {props.notification.map(notification => (
        <div className="container" key={notification.id}>
          <div className="row justify-content-end">
            <div className="col-6">
              <div className={`alert alert-${notification.level}`} role="alert">
                <i className={`fa ${notification.icon} mr-1`} aria-hidden="true" />
                <strong>{notification.title}</strong> - {notification.content}
              </div>
            </div>
          </div>
        </div>
      ))}
    </ReactCSSTransitionGroup>
  </div>
);

Notification.propTypes = {
  notification: PropTypes.array,
};

Notification.defaultProps = {
  notification: [],
};

export default Notification;
