import React, { Component } from 'react';
import Navigation from '../Navigation';
import Notification from '../../container/Notification';
import ModalRoot from '../../container/Modal/modalroot';

class Layout extends Component {
  render() {
    return (
      <div>
        <ModalRoot />
        <Navigation />
        <div className="container">
          <Notification />
          <header />
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Layout;
