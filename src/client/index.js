import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import cookie from 'js-cookie';
import { Provider } from 'react-redux';
import { composeWithDevTools as devTools } from 'redux-devtools-extension';
import reducer from '../shared/redux/reducer';
import Layout from './component/Layout';
import Home from './container/Home';
import About from './component/About';
import Signup from './container/Signup';
import Login from './container/Login';
import ForgotPassword from './container/ForgotPassword';
import ResetPassword from './container/ResetPassword';
import ValidateEmail from './container/ValidateEmail';
import Dashboard from './container/Dashboard';
import Content from './container/Content';
import Activate from './container/Activate';
import NoMatch from './component/NoMatch';
import Authentication from './container/Authentication';
import { AUTH_LOGIN } from '../shared/redux/actions/types';
import '../../assets/styles/scss/index.scss';

// const composeEnhancers = devTools({
//   // Specify name here, actionsBlacklist, actionsCreators and other options if needed
// });

const store = createStore(reducer, devTools(applyMiddleware(reduxThunk)));

const token = cookie.get('token');

if (token) {
  store.dispatch({ type: AUTH_LOGIN });
}

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Layout}>
        <IndexRoute component={Home} />
        <Route path="signup" component={Signup} />
        <Route path="login" component={Login} />
        <Route path="forgot-password" component={ForgotPassword} />
        <Route path="reset-password/:token" component={ResetPassword} />
        <Route path="validate-email/:token" component={ValidateEmail} />
        <Route path="about" component={About} />
        <Route path="activate/:token" component={Activate} />
        <Route path="content/create" component={Content} />
        <Route path="content/edit/:id" component={Content} />
        <Route path="dashboard" component={Authentication(Dashboard)} />
        <Route path="*" component={NoMatch} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
