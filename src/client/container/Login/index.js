import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoginForm from '../../component/LoginForm';
import { loginUser } from '../../../shared/redux/actions/authentication';

class Login extends Component {
  handleFormSubmit(formProps) {
    this.props.loginUser(formProps.email, formProps.password);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <span>
            <strong>error: </strong> {this.props.errorMessage}
          </span>
        </div>
      );
    }
  }

  render() {
    return <LoginForm onSubmit={this.handleFormSubmit.bind(this)} />;
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
  };
}

export default connect(mapStateToProps, { loginUser })(Login);
