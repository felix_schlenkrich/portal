import React, { Component } from 'react';
import { connect } from 'react-redux';
import ValidateEmail from '../../component/ValidateEmail';
import { validateEmail } from '../../../shared/redux/actions/authentication';

class ValidateEmailContainer extends Component {
  componentDidMount() {
    console.log('componentDidMount: send request  with token');
  }

  render() {
    return <ValidateEmail />;
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
  };
}

export default connect(mapStateToProps, { validateEmail })(ValidateEmailContainer);
