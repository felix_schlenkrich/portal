import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signupUser } from '../../../shared/redux/actions/authentication';
import SignupForm from '../../component/SignupForm';

class Signup extends Component {
  handleFormSubmit(formProps) {
    this.props.signupUser(
      formProps.email,
      formProps.username,
      formProps.password,
      formProps.newsletter,
    );
  }

  render() {
    return <SignupForm onSubmit={this.handleFormSubmit.bind(this)} />;
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
  };
}

export default connect(mapStateToProps, { signupUser })(Signup);
