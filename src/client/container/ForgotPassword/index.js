import React, { Component } from 'react';
import { connect } from 'react-redux';
import ForgotPasswordForm from '../../component/ForgotPassword';
import { forgotPassword } from '../../../shared/redux/actions/authentication';

class ForgotPassword extends Component {
  handleFormSubmit(formProps) {
    this.props.forgotPassword(formProps.email);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <span>
            <strong>error: </strong> {this.props.errorMessage}
          </span>
        </div>
      );
    }
  }

  render() {
    return <ForgotPasswordForm onSubmit={this.handleFormSubmit.bind(this)} />;
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
  };
}

export default connect(mapStateToProps, { forgotPassword })(ForgotPassword);
