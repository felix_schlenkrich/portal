import React, { Component } from 'react';
import { connect } from 'react-redux';
import Home from '../../component/Home';
import generatePageTitle from '../../../../utils/generatePageTitle';

class HomeContainer extends Component {
  componentDidMount() {
    document.title = generatePageTitle('Home');
  }

  render() {
    return <Home />;
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
  };
}

export default connect(mapStateToProps)(HomeContainer);
