import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../component/Dashboard';
import { list, remove as removeContent } from '../../../shared/redux/actions/content';
import { showModal } from '../../../shared/redux/actions/modal';
import {
  MODAL_TYPE_CONFIRMATION,
  MODAL_TYPE_NOTIFICATION,
} from '../../../shared/redux/constants/modaltypes';
import generatePageTitle from '../../../../utils/generatePageTitle';

class DashboardContainer extends Component {
  componentDidMount() {
    this.props.list();
    document.title = generatePageTitle('Dashboard');
  }

  showNotification() {
    this.props.showModal(MODAL_TYPE_NOTIFICATION, {
      title: 'Notification',
      body: 'This is an awesome notification.',
    });
  }

  showConfirmation() {
    this.props.showModal(MODAL_TYPE_CONFIRMATION, {
      title: 'Confirmation',
      body: 'Do you confirm?',
      onConfirm: (isConfirmed) => {
        this.props.showModal(MODAL_TYPE_NOTIFICATION, {
          title: 'Notification',
          body: `The user did confirm: ${isConfirmed}`,
        });
      },
    });
  }

  removeContent(_id) {
    this.props.showModal(MODAL_TYPE_CONFIRMATION, {
      title: 'Delete',
      body: 'Do you really want to delete this item?',
      onConfirm: (isConfirmed) => {
        if (isConfirmed) this.props.removeContent(_id);
      },
    });
  }

  render() {
    return (
      <Dashboard
        list={this.props.content}
        showNotification={this.showNotification.bind(this)}
        showConfirmation={this.showConfirmation.bind(this)}
        removeContent={this.removeContent.bind(this)}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    content: state.content.list,
  };
}

export default connect(mapStateToProps, { list, showModal, removeContent })(DashboardContainer);
