import React, { Component } from 'react';
import { connect } from 'react-redux';
import ResetPasswordForm from '../../component/ResetPassword';
import { resetPassword } from '../../../shared/redux/actions/authentication';

class ResetPassword extends Component {
  handleFormSubmit(formProps) {
    this.props.resetPassword(this.props.params.token, formProps.password);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <span>
            <strong>error: </strong> {this.props.errorMessage}
          </span>
        </div>
      );
    }
  }

  render() {
    return <ResetPasswordForm onSubmit={this.handleFormSubmit.bind(this)} />;
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
  };
}

export default connect(mapStateToProps, { resetPassword })(ResetPassword);
