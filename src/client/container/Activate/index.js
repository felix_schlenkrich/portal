import React, { Component } from 'react';
import { connect } from 'react-redux';
import Activate from '../../component/Activate';
import { activateEmail } from '../../../shared/redux/actions/authentication';

class ActivateContainer extends Component {
  componentDidMount() {
    this.props.activateEmail(this.props.params.token);
  }
  render() {
    return <Activate message={this.props.message} />;
  }
}

function mapStateToProps(state) {
  return {
    message: state.auth.activateEmail.message,
  };
}

export default connect(mapStateToProps, { activateEmail })(ActivateContainer);
