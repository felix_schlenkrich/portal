import React from 'react';
import { connect } from 'react-redux';

import { hideModal } from '../../../shared/redux/actions/modal';
import Modal from './';

const Confirmation = ({ title, body, onConfirm, hideModal }) => {
  const onClose = () => {
    hideModal();

    // add close callback?
    // if (afterClose) {
    //  afterClose();
    // }
  };

  const handleConfirm = isConfirmed => () => {
    hideModal();
    onConfirm(isConfirmed);
  };

  return (
    <Modal title={title} onClose={onClose}>
      <div className="mb-3">{body}</div>
      <button className="btn btn-primary mr-3 px-3" tabIndex={0} onClick={handleConfirm(true)}>
        Yes
      </button>
      <button className="btn btn-secondary px-3" tabIndex={0} onClick={handleConfirm(false)}>
        No
      </button>
    </Modal>
  );
};

export default connect(null, { hideModal })(Confirmation);
