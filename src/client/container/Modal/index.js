import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Modal extends Component {
  componentDidMount() {
    if (this.props.onClose) window.addEventListener('keydown', this.listenKeyboard.bind(this), true);
  }

  onDialogClick(event) {
    event.stopPropagation();
  }

  onOverlayClick() {
    this.props.onClose();
  }

  get title() {
    const { title } = this.props;

    return title ? (
      <div className="modal-title text-light">
        {title}
      </div>
    ) : null;
  }

  get close() {
    const { onClose } = this.props;

    return onClose ? (
      <div className="modal-close mt-1 mr-2 pointer" role="button" tabIndex={0} onClick={onClose}>
        <i className="fa fa-times text-light" aria-hidden="true" />
      </div>
    ) : null;
  }

  listenKeyboard(event) {
    if (event.key === 'Escape' || event.keyCode === 27) this.props.onClose();
  }

  componentWillUnount() {
    if (this.props.onClose) window.removeEventListener('keydown', this.listenKeyboard, true);
  }

  render() {
    return (
      <div className="modal">
        <div className="overlay" />
        <div className="content" role="presentation" onClick={this.onOverlayClick.bind(this)}>
          <div className="dialog" role="presentation" onClick={this.onDialogClick.bind(this)}>
            <div className="header d-flex flex-row justify-content-between pl-2 mb-3 bg-primary">
              <div className="d-flex align-items-center p-1">{this.title}</div>
              {this.close}
            </div>
            <div className="body">{this.props.children}</div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  onClose: PropTypes.func,
};

export default Modal;
