import React from 'react';
import { connect } from 'react-redux';

import { hideModal } from '../../../shared/redux/actions/modal';
import Modal from './';

const Notification = ({ title, body, afterClose, hideModal }) => {
  const onClose = () => {
    hideModal();

    if (afterClose) {
      afterClose();
    }
  };

  return (
    <Modal title={title} onClose={onClose}>
      <div className="mb-3">{body}</div>
      <button className="btn btn-primary btn-sm px-3" onClick={onClose}>
        Close
      </button>
    </Modal>
  );
};

export default connect(null, { hideModal })(Notification);
