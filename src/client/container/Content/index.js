import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContentForm from '../../component/Content/form';
import { add, update, get } from '../../../shared/redux/actions/content';

class Content extends Component {
  componentDidMount() {
    this.props.get(this.props.params.id);
  }

  handleFormSubmit(formProps) {
    if (this.props.params.id) this.props.update(this.props.params.id, formProps.title, formProps.body);
    else this.props.add(formProps.title, formProps.body);
  }

  render() {
    return (<ContentForm
      onSubmit={this.handleFormSubmit.bind(this)}
      initialValues={this.props.initialValues}
    />);
  }
}

function mapStateToProps(state, props) {
  return {
    initialValues: state.content.item,
  };
}

export default connect(mapStateToProps, { add, update, get })(Content);
