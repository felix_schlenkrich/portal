import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Notification from '../../component/Notification';
import { NOTIFICATION_ADD, NOTIFICATION_REMOVE } from '../../../shared/redux/actions/types';
import { Notification as NotificationTest } from '../../../shared/helper/Notification';

class NotificationContainer extends Component {
  componentDidMount() {
    /*
    const notification = new NotificationTest(
      'info',
      'Network info',
      'Try downloading the file from another network connection.',
      'fa-info-circle',
    );
    const notification2 = new NotificationTest(
      'danger',
      'Network danger',
      'Try downloading the file from another network connection.',
      'fa-times-circle',
    );
    const notification3 = new NotificationTest(
      'warning',
      'System warning',
      'Try downloading the file from another network connection.',
      'fa-exclamation-circle ',
    );
    const notification4 = new NotificationTest(
      'success',
      'Network success',
      'Try downloading the file from another network connection.',
      'fa-thumbs-o-up',
    );
    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_ADD, notification });
    }, 1000);

    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_ADD, notification: notification2 });
    }, 2000);
    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_ADD, notification: notification3 });
    }, 3000);
    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_ADD, notification: notification4 });
    }, 4000);

    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_REMOVE, notification });
    }, 6000);

    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_REMOVE, notification: notification2 });
    }, 7000);
    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_REMOVE, notification: notification3 });
    }, 8000);
    setTimeout(() => {
      this.props.dispatch({ type: NOTIFICATION_REMOVE, notification: notification4 });
    }, 9000);
    */
  }

  componentWillReceiveProps(nextProps) {
    this.render();
  }

  componentWillUnmount() {
    // remove event listener
  }

  render() {
    return <Notification notification={this.props.notification} />;
    // return <div>jo</div>;
  }
}

NotificationContainer.propTypes = {
  notification: PropTypes.array,
  dispatch: PropTypes.func.isRequired,
};

NotificationContainer.defaultProps = {
  notification: [],
};

function mapStateToProps(state) {
  return { notification: state.notification };
}

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationContainer);
