import path from 'path';
import { spawn } from 'child_process';
import logger from '../../utils/logger';

class ExpressServer {
  constructor(compiler, clientCompiler, port, entry) {
    logger.info('Constructing express server.');

    logger.info(`compiler.options.entry: ${compiler.options.entry}`);

    const compiledEntryFile = path.join(compiler.options.output.path, entry);

    const startServer = () => {
      if (this.server) {
        this.server.kill();
        this.server = null;
        logger.info('Restarting server...');
      }
      const inspect = `--inspect=${port}`;

      const options = [];
      if (process.env.NODE_ENV === 'debug') options.push(inspect);
      options.push(compiledEntryFile);
      const newServer = spawn('node', options);
      logger.info('Server running with latest changes.');

      newServer.stdout.on('data', (data) => {
        /* eslint no-console: "off" */
        console.log(data.toString().trim());
      });
      newServer.stderr.on('data', (data) => {
        logger.info('Error in server execution, check the console for more info.');
        data.toString().trim().split(/\r?\n/).forEach((line) => {
          logger.info(line);
        });
      });
      newServer.on('close', (code) => {
        logger.warn(`Child process exited with code ${code}`);
      });

      this.server = newServer;
    };

    const waitForClientThenStartServer = () => {
      if (this.serverCompiling) {
        // A new server bundle is building, break this loop.
        return;
      }
      if (this.clientCompiling) {
        setTimeout(waitForClientThenStartServer, 50);
      } else {
        startServer();
      }
    };

    clientCompiler.plugin('compile', () => {
      this.clientCompiling = true;
    });

    clientCompiler.plugin('done', (stats) => {
      if (!stats.hasErrors()) {
        this.clientCompiling = false;
      }
    });

    compiler.plugin('done', (stats) => {
      this.serverCompiling = false;

      if (this.disposing) {
        return;
      }

      try {
        if (stats.hasErrors()) {
          logger.error('Build failed, check the console for more information.');
          logger.error(stats.toString());
          return;
        }

        waitForClientThenStartServer();
      } catch (err) {
        logger.error('Failed to start, please check the console for more information.');
        logger.error(err);
      }
    });
    this.watcher = compiler.watch(null, () => undefined);
  }
  dispose() {
    logger.info('Disposing express server.');
    this.disposing = true;
    const stopWatcher = new Promise((resolve) => {
      this.watcher.close(resolve);
    });
    return stopWatcher.then(() => {
      if (this.server) this.server.kill();
    });
  }
}

export default ExpressServer;
