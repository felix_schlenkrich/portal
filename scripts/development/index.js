import chokidar from 'chokidar';
import path from 'path';
import appRootDir from 'app-root-dir';
import logger from '../../utils/logger';

let DevelopmentServer = require('./development-server').default;

let developmentServer = new DevelopmentServer();

if (process.argv.includes('--debug')) process.env.NODE_ENV = 'debug';
else if (process.argv.includes('--development')) process.env.NODE_ENV = 'development';

// Watch all build and config scripts for changes
const watcher = chokidar.watch([
  path.resolve(appRootDir.get(), 'scripts'),
  path.resolve(appRootDir.get(), 'config'),
  path.resolve(appRootDir.get(), 'webpack'),
]);

watcher.on('ready', () => {
  watcher.on('change', () => {
    logger.info('Project build configuration changed.');
    logger.info('Restarting the development server.');

    developmentServer.dispose().then(() => {
      /* delete webpack bundle config from module cache */
      Object.keys(require.cache).forEach((modulePath) => {
        if (modulePath.indexOf('config') !== -1) {
          delete require.cache[modulePath];
        } else if (modulePath.indexOf('scripts') !== -1) {
          delete require.cache[modulePath];
        } else if (modulePath.indexOf('webpack') !== -1) {
          delete require.cache[modulePath];
        }
      });
      /* Re-require the DevelopmentServer, so that all new config are used */
      // eslint-disable-next-line global-require
      DevelopmentServer = require('./development-server').default;
      developmentServer = new DevelopmentServer();
    });
  });
});

process.on(
  'SIGTERM',
  () => developmentServer && developmentServer.dispose().then(() => process.exit(0)),
);
