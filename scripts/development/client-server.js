import express from 'express';
import createWebpackMiddleware from 'webpack-dev-middleware';
import createWebpackHotMiddleware from 'webpack-hot-middleware';
import ListenManager from './listenManager';
import logger from '../../utils/logger';
import config from '../../config';

class ClientServer {
  constructor(compiler) {
    logger.info('Constructing client server.');

    const app = express();

    this.compiler = compiler;
    this.webpackDevMMiddleware = createWebpackMiddleware(compiler, {
      quiet: true,
      noInfo: true,
      timings: false,
      stats: {
        colors: true,
      },
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      publicPath: compiler.options.output.publicPath,
    });

    app.use(this.webpackDevMMiddleware);
    app.use(createWebpackHotMiddleware(compiler));

    const listener = app.listen(config.network.portBundleServer, config.network.host);

    logger.info(`Bundle server listeing on port ${config.network.portBundleServer}`);

    compiler.plugin('compile', () => {
      logger.info('Building new bundle.');
    });

    compiler.plugin('done', (stats) => {
      if (stats.hasErrors()) {
        logger.info('Build failed.');
        logger.info(stats.toString());
      } else {
        logger.info('Running with the latest changes');
      }
    });

    this.listenManager = new ListenManager(listener);
  }

  dispose() {
    logger.info('Disposing client server.');
    this.webpackDevMMiddleware.close();

    return this.listenManager ? this.listenManager.dispose() : Promise.resolve();
  }
}

export default ClientServer;
