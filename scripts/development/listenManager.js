import logger from '../../utils/logger';

class ListenManager {
  constructor(listener) {
    this.lastConnectionKey = 0;
    this.connectionMap = {};
    this.listener = listener;

    // Track all connections to server and close them when needed
    this.listener.on('connection', (connection) => {
      this.lastConnectionKey += 1;
      const connectionKey = this.lastConnectionKey;
      this.connectionMap[connectionKey] = connection;

      connection.on('close', () => {
        delete this.connectionMap[connectionKey];
      });
    });
  }

  killAllConnections() {
    Object.keys(this.connectionMap).forEach((connectionKey) => {
      this.connectionMap[connectionKey].destroy();
    });
  }

  dispose() {
    return new Promise((resolve) => {
      if (this.listener) {
        this.killAllConnections();

        logger.info('Destroyed all connections.');

        this.listener.close(() => {
          logger.info('Closed the listener.');
        });
      }
      resolve();
    });
  }
}

export default ListenManager;
