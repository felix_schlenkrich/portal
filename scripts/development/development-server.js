import webpack from 'webpack';
import ClientServer from './client-server';
import ExpressServer from './express-server';
import logger from '../../utils/logger';
import webpackClientConfig from '../../webpack/webpack.client.config';
import webpackServerConfig from '../../webpack/webpack.server.config';
import webpackApiConfig from '../../webpack/webpack.api.config';
import config from '../../config';

class DevelopmentServer {
  constructor() {
    this.clientServer = null;
    this.expressServer = null;
    this.apiServer = null;

    logger.info('Creating webpack compiler for client bundle.');
    const clientCompiler = this.createCompiler(webpackClientConfig);
    this.clientServer = new ClientServer(clientCompiler);

    logger.info('Creating webpack compiler for server bundle.');
    this.expressServer = new ExpressServer(
      this.createCompiler(webpackServerConfig),
      clientCompiler,
      config.bundleConfig.express.debugPort,
      config.bundleConfig.express.filename,
    );
    logger.info('Creating webpack compiler for API Server.');
    this.apiServer = new ExpressServer(
      this.createCompiler(webpackApiConfig),
      clientCompiler,
      config.additionalNodeBundles.api.debugPort,
      config.additionalNodeBundles.api.filename,
    );
  }

  // eslint-disable-next-line class-methods-use-this
  createCompiler(webPackConfig) {
    try {
      return webpack(webPackConfig);
    } catch (err) {
      logger.info('Webpack config is invalid.');
      logger.error(err);
      throw err;
    }
  }
  dispose() {
    const disposer = server => (server ? server.dispose() : Promise.resolve());

    return disposer(this.clientServer).then(() =>
      disposer(this.expressServer).then(disposer(this.apiServer)),
    );
  }
}

export default DevelopmentServer;
