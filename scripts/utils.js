import notifier from 'node-notifier';
import colors from 'colors/safe';

/* eslint-disable import/prefer-default-export */
export function log(options) {
  const title = options.title.toUpperCase();

  if (options.notify) {
    notifier.notify({
      title,
      message: options.message,
    });
  }

  const level = options.level || 'info';
  let message = '';

  /* eslint-disable default-case */
  switch (level) {
    case 'info':
      message += colors.grey(`${level.toUpperCase()}  `);
      break;
    case 'success':
      message += colors.green(level.toUpperCase());
      break;
    case 'warn':
      message += colors.yellow(`${level.toUpperCase()}  `);
      break;
    case 'error':
      message += colors.red(`${level.toUpperCase()} `);
      break;
  }

  const dateNow = new Date();
  const dateString = `${dateNow.getHours()}:${dateNow.getMinutes()}:${dateNow.getSeconds()}`;

  message += ` ${dateString} <${options.title}> ==> ${options.message}`;
  /* eslint-disable  no-console */
  console.log(message);
}
