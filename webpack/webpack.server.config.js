const path = require('path');
const webpack = require('webpack');
const appRootDir = require('app-root-dir');

/* Test */
module.exports = {
  target: 'node',
  devtool: 'source-map',
  entry: {
    index: ['source-map-support/register', path.resolve(appRootDir.get(), './src/server/app.js')],
  },
  output: {
    path: path.resolve(appRootDir.get(), './build/server'),
    filename: 'server.js',
    publicPath: 'http://localhost:4000/client/',
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
  },
  resolve: { extensions: ['.js', '.jsx', '.json'] },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_mdoules/,
        include: [
          path.resolve(appRootDir.get(), './src/server'),
          path.resolve(appRootDir.get(), './src/shared'),
          path.resolve(appRootDir.get(), './config'),
        ],
        loader: 'babel-loader',
        query: {
          presets: ['react', 'stage-3', ['env', { targets: { node: true }, modules: false }]],
          plugins: [],
        },
      },
    ],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    // do not emit compiled assets that include errors
  ],
};
