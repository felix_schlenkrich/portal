const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const appRootDir = require('app-root-dir');
const config = require('../config');

const nodeModules = {};
fs.readdirSync('node_modules').filter(x => ['.bin'].indexOf(x) === -1).forEach((mod) => {
  nodeModules[mod] = `commonjs ${mod}`;
});

module.exports = {
  target: 'node',
  devtool: 'source-map',
  entry: {
    index: [
      'source-map-support/register',
      path.resolve(appRootDir.get(), config.additionalNodeBundles.api.entry),
    ],
  },
  output: {
    path: path.resolve(appRootDir.get(), config.additionalNodeBundles.api.outputPath),
    filename: 'api.js',
    publicPath: '/',
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
  },
  resolve: { extensions: ['.js', '.json'] },
  externals: [nodeModules],
  module: {
    rules: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        include: [
          path.resolve(appRootDir.get(), './src/api'),
          path.resolve(appRootDir.get(), './src/shared'),
          path.resolve(appRootDir.get(), './config'),
        ],
        query: {
          presets: ['stage-3', ['env', { targets: { node: true }, modules: false }]],
        },
      },
    ],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    // do not emit compiled assets that include errors
  ],
};
