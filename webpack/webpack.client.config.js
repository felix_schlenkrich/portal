const path = require('path');
const webpack = require('webpack');
const appRootDir = require('app-root-dir');
const config = require('../config');

module.exports = {
  target: 'web',
  devtool: 'source-map',
  entry: [
    'react-hot-loader/patch',
    `webpack-hot-middleware/client?reload=true&path=http://${config.network.host}:${config.network
      .portBundleServer}/__webpack_hmr`,
    path.resolve(appRootDir.get(), './src/client/index.js'),
  ],
  resolve: { extensions: ['.js', '.jsx', '.json'] },
  output: {
    path: path.resolve(appRootDir.get(), config.bundleConfig.client.outputPath),
    libraryTarget: 'var',
    publicPath: 'http://localhost:4000/client/',
    filename: 'index.js',
  },
  module: {
    rules: [
      {
        test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        include: [
          path.resolve(appRootDir.get(), './src/client'),
          path.resolve(appRootDir.get(), './src/shared'),
          path.resolve(appRootDir.get(), './config'),
        ],
        query: {
          presets: ['react', 'stage-3', ['latest', { es2015: { modules: false } }]],
        },
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader?name=fonts/[name].[ext]',
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),
    // prints more readable module names in the browser console on HMR updates

    new webpack.NoEmitOnErrorsPlugin(),
    // do not emit compiled assets that include errors
  ],
};
