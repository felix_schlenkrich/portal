module.exports = function (config) {
  config.set({
    preprocessors: {
      'src/client/**/*.js': ['babel'],
      'src/shared/**/*.js': ['babel'],
    },
    babelPreprocessor: {
      options: {
        presets: ['env', 'stage-3', 'react'],
        sourceMap: 'inline',
      },
    },
    frameworks: ['mocha', 'chai', 'requirejs'],
    files: ['test/**/*.js'],
    reporters: ['progress'],
    port: 9876, // karma web server port
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ['ChromeHeadless'],
    autoWatch: false,
    // singleRun: false, // Karma captures browsers, runs the tests and exits
    concurrency: Infinity,
    plugins: [
      'karma-requirejs',
      'karma-mocha',
      'karma-chai',
      'karma-babel-preprocessor',
      'karma-chrome-launcher',
    ],
  });
};
